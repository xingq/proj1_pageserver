# README #

## Author: Xing Qian, xingq@uoregon.edu ##

This is a solution of the assignment "getting started" project for CIS 322, introduction to software engineering, at the University of Oregon.

### Run the test ###

If you want to run the test, please do the following steps:

At first, you need to run the server.
```
make start
```

Then, you just need run the next command for testing.
```
./tests/tests.sh http://127.0.0.1:5000
```
